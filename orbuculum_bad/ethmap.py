from orbuculum_bad import OrbuculumEtherClient
from subprocess import PIPE, run
from pathlib import Path
import sys
import warnings

warnings.filterwarnings("ignore")

def get_connection_mapping():
    """Get the mapping of the network cards"""
    try:
        client = OrbuculumEtherClient("127.0.0.1:15051")
        connection_mapping = client.get_connection_mapping()
    except ConnectionRefusedError:
        sys_net_path = Path("/sys/class/net")
        connection_mapping = {str(x): str(x) for x in sys_net_path.iterdir() if x.is_dir()}

    return connection_mapping


def convert_sysargs():
    """Get all the arguments from the command-line convert it to list
    from network cards' mapping"""
    sysargs = sys.argv[1:]
    interface_mapping = get_connection_mapping()

    new_args = []
    found = False
    for arg in sysargs:
        if arg in interface_mapping:
            found = True
            new_args.append(interface_mapping[arg])
        else:
            new_args.append(arg)

    return (new_args, found)


def ethmap():
    """Print ifconfig with replaced contents"""
    args, found = convert_sysargs()
    res = []
    final_map = {}
    count = 0

    try:
        proc = run(args, stdout=PIPE)
    except FileNotFoundError:
        print(f"{args[0]}: command not found")
        sys.exit(127)
    except KeyboardInterrupt:
        sys.exit(1)

    output = proc.stdout.decode()

    for connection, interface in get_connection_mapping().items():
        count += 1
        if output.count(interface) == 1:
            output_mid = output.replace(interface, connection)
            if output_mid.count(connection) == 2:
                tmp_name = "AABBCC" + str(count)
                output_mid = output.replace(interface, tmp_name)
                final_map[tmp_name] = connection
            output = output_mid

    for key, value in final_map.items():
        output = output.replace(key, value)

    if output:
        print(output)


ethmap()
