# python-orbuculum-bad

A really simple library for commercial use of orbuculum.

## Usage

```python
from orbuculum_bad import OrbuculumEtherClient

client = OrbuculumEtherClient("localhost:15051")

client.get_first_port()
client.get_last_port()
```

* License: MIT
